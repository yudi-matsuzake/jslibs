/* Last edited on 2021-06-14 01:35:49 by jstolfi */ 
/* Test of the PPV library. */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <bool.h>
#include <indexing.h>
#include <indexing_io.h>

#include <ppv_array.h>

#define posFMT ppv_pos_t_FMT
#define smpFMT ppv_sample_t_FMT
#define ixFMT  ppv_index_t_FMT
#define szFMT  ppv_size_t_FMT
#define stFMT  ppv_step_t_FMT
#define sctFMT ppv_sample_count_t_FMT

#define bug(FMT,...) \
  do { \
    fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); \
    fprintf(stderr, "** " FMT "\n", __VA_ARGS__); \
    exit(1); \
  } while(0)

#define bug0(MSG) \
  do { \
    fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); \
    fputs(MSG, stderr); \
    fprintf(stderr, "\n"); \
    exit(1); \
  } while(0)

void do_tests(ppv_dim_t d, ppv_nbits_t bps, ppv_nbits_t bpw);

ppv_sample_t ransample(ppv_dim_t d, ppv_index_t ix[], int32_t K, int32_t L, ppv_nbits_t bps);
  /* A pseudorandom sample value that depends on the index vector {ix[0..d-1]}
    and on the parameters K and L.  The sample will be in the range
    {0..2^bps-1}. */
    
void enum_by_hand(ix_index_op_t op, ppv_array_desc_t *A);
  /* Calls {op(ix)} on all index vectors of the array {A}, in lex order.
    The {op} argument must be a procedure of type {ix_index_op_t}, that
    receives an index vector and returns a {bbol_t} ({TRUE} to stop the
    enumration). */

void test_new_array(ppv_array_desc_t *A, ppv_size_t *sz, ppv_nbits_t bps, ppv_nbits_t bpw);
void test_packing(ppv_array_desc_t *A);
void test_sample_pos(ppv_array_desc_t *A);
void test_enum(ppv_array_desc_t *A);
void test_assign(ppv_array_desc_t *A, ppv_nbits_t bpsB);

void test_crop(ppv_array_desc_t *A);
void test_subsample(ppv_array_desc_t *A);
void test_reverse(ppv_array_desc_t *A);
void test_replicate(ppv_array_desc_t *A);
void test_swap_indices(ppv_array_desc_t *A);
void test_flip_indices(ppv_array_desc_t *A);
void test_slice(ppv_array_desc_t *A);
void test_diagonal(ppv_array_desc_t *A);
void test_chop(ppv_array_desc_t *A);

void dump_storage(FILE *wr, void *el, int nw, ppv_nbits_t bps, ppv_nbits_t bpw);
void check_size(ppv_dim_t d, ppv_size_t *sza, ppv_size_t *szb);

int main (int argn, char **argv)
  {
    do_tests(2, 10,  8);
    do_tests(2, 10, 32);
    do_tests(5,  0, 16);
    do_tests(1,  1,  8);
    do_tests(6,  7, 16);
    do_tests(0, 10, 16);
    return 0;
  }

ppv_sample_t ransample(ppv_dim_t d, ppv_index_t ix[], int32_t K, int32_t L, ppv_nbits_t bps)
  { ppv_sample_t smax = (ppv_sample_t)((1 << bps) - 1);
    double s = 0;
    for (ppv_axis_t ax = 0; ax < d; ax++)
      { s += sin(K*ax + L)*((double)ix[ax]); }
    s = s - floor(s);
    ppv_sample_t smp = (ppv_sample_t)floor(pow(2,bps)*s);
    assert((smp >= 0) && (smp <= smax));
    return smp;
  }

void do_tests(ppv_dim_t d, ppv_nbits_t bps, ppv_nbits_t bpw)
  {
    fprintf(stderr, "--- {do-tests} d = %d bps = %d bpw = %d ---\n", d, bps, bpw);

    ppv_dim_t dmax = 6;
    assert(d <= dmax);
    ppv_size_t szmax[6] = { 3, 10, 20, 30, 7, 2 };
    
    /* Create the array: */
    ppv_size_t sz[d];
    for (ppv_axis_t ax = 0; ax < d; ax++) { sz[ax] = szmax[ax]; }
    ppv_array_desc_t *A = ppv_array_new(d, sz,  bps, bpw);

    test_new_array(A, sz, bps, bpw);
    test_packing(A);
    test_sample_pos(A);
    
    test_enum(A);
    test_assign(A, A->bps);
    if (A->bps < A->bpw) { test_assign(A, (ppv_nbits_t)(A->bps+1)); }
    if (A->bps > 0) { test_assign(A, (ppv_nbits_t)(A->bps-1)); }
    
    test_crop(A);
    test_subsample(A);
    test_reverse(A);
    test_replicate(A);
    test_swap_indices(A);
    test_flip_indices(A);
    test_slice(A);
    test_diagonal(A);
    test_chop(A);
    
    fprintf(stderr, "done.\n");
    return;
  }

void enum_by_hand(ix_index_op_t op, ppv_array_desc_t *A)
  {
    ppv_dim_t dmax = 6;
    ppv_dim_t d = A->d;
    assert(d <=dmax);
    
    ppv_size_t szA[dmax];  /* Vector {A->size} extended with 1s to {dmax} entries. */
    for (ppv_axis_t ax = 0; ax < dmax; ax++) { szA[ax] = (ax < d ? A->size[ax] : 1); }
    ppv_index_t ixA[dmax];
    for (ixA[5] = 0; ixA[5] < szA[5]; ixA[5]++)
      for (ixA[4] = 0; ixA[4] < szA[4]; ixA[4]++)
        for (ixA[3] = 0; ixA[3] < szA[3]; ixA[3]++)
          for (ixA[2] = 0; ixA[2] < szA[2]; ixA[2]++)
            for (ixA[1] = 0; ixA[1] < szA[1]; ixA[1]++)
              for (ixA[0] = 0; ixA[0] < szA[0]; ixA[0]++)
                { bool_t stop = op(ixA);
                  if (stop) { return; }
                }
    return;
  }

void test_new_array(ppv_array_desc_t *A, ppv_size_t *sz, ppv_nbits_t bps, ppv_nbits_t bpw)
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking num of axes...\n");

    fprintf(stderr, "Checking {ppv_array_new,ppv_descriptor_is_valid}...\n");
    bool_t verbose = TRUE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    if (! ppv_descriptor_is_valid(A, TRUE)) { bug0("{ppv_array_new}: returns invalid desc"); }
    if (A->base != 0) { bug("base = " posFMT, A->base); }
    if (A->bps != bps) { bug("bpw = %u", A->bps); }
    if (A->bpw != bpw) { bug("bpw = %u", A->bpw); }
    /* Check steps for a vanilla array: */
    ppv_sample_count_t stp = 1;
    for (ppv_axis_t ax = 0; ax < d; ax++) 
      { if (A->step[ax] != stp) { bug("step[%d] = "  stFMT, ax, A->step[ax]); }
        stp *= sz[ax];
      }

    fprintf(stderr, "Checking {ppv_index_is_valid}...\n");
    ppv_index_t ixA[d];
    ppv_index_clear(d, ixA);
    if (! ppv_index_is_valid(ixA, A)) { bug0("bogus ppv_index_is_valid"); } 
    for (ppv_axis_t ax = 0; ax < d; ax++)
      { ixA[ax] = sz[ax]-1;
        if (! ppv_index_is_valid(ixA, A)) { bug0("bogus ppv_index_is_valid"); } 
        ixA[ax] = sz[ax];
        if (ppv_index_is_valid(ixA, A)) { bug0("bogus ppv_index_is_valid"); } 
        ixA[ax] = 0;
      }
  }

void test_packing(ppv_array_desc_t *A)
  {
    fprintf(stderr, "Checking {ppv_{get,set}_sample_at_pos} on a few elems...\n");
    bool_t verbose = TRUE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    ppv_word_t smask = ((ppv_word_t)1 << A->bps) - 1;
    if (verbose) { fprintf(stderr, "bps = %u mask = " smpFMT "\n", A->bps, smask); }
    int npos = 5; /* Number of positions to test */
    int ndump = (npos*A->bps + A->bpw - 1)/A->bpw + 1; /* Number of words to dump. */
    fprintf(stderr, "\n");
    for (ppv_pos_t pos = 0; pos < npos; pos++)
      { ppv_sample_t xsmp = (ppv_sample_t)(smask & ((2*pos) | 1 | (1LU <<(A->bps-1))));
        if (verbose) { fprintf(stderr, "setting sample " posFMT " to %u\n", pos, xsmp); }
        ppv_set_sample_at_pos(A->el, A->bps, A->bpw, pos, xsmp);
        if (verbose) { dump_storage(stderr, A->el, ndump, A->bps, A->bpw); }
        if (verbose) { fprintf(stderr, "\n"); }
        ppv_sample_t smp = ppv_get_sample_at_pos(A->el, A->bps, A->bpw, pos);
        if (smp != xsmp) 
          { bug("set/get sample mismatch smp = " smpFMT " xsmp = " smpFMT, smp, xsmp); } 
      }
    fprintf(stderr, "\n");
  }

void test_sample_pos(ppv_array_desc_t *A)
  {
    fprintf(stderr, "Checking {ppv_sample_pos,ppv_{get,set}_sample{,_at_pos}} on all elems...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    
    ppv_dim_t d = A->d;

    fprintf(stderr, "  filling array...\n");
    auto bool_t fill1(ppv_index_t ixA[]);
      /* Checks {ppv_sample_pos}, sets/gets/compares each sample. */ 
    ppv_pos_t xpos = 0;
    enum_by_hand(fill1, A);
    
    fprintf(stderr, "  re-checking the value of all samples...\n");
    auto bool_t check1(ppv_index_t ixA[]);
      /* Checks if samples set by {fill1} are still there. */ 
    enum_by_hand(check1, A);
    
    return;
    
    bool_t fill1(ppv_index_t ixA[])
      { 
        if (! ppv_index_is_valid(ixA, A)) { bug0("not ppv_index_is_valid"); } 
        ppv_pos_t pos = ppv_sample_pos(A, ixA);
        if (pos != xpos) 
          { bug("{ppv_sample_pos}: mismatch pos = " posFMT " xpos = " posFMT, pos, xpos); } 

        ppv_sample_t smp1 = ransample(d, ixA, 3, +17, A->bps);
        ppv_set_sample_at_pos(A->el, A->bps, A->bpw, pos, smp1);
        ppv_sample_t xsmp = ppv_get_sample_at_pos(A->el, A->bps, A->bpw, pos);
        if (xsmp != smp1) 
          { bug("{set_sample_at_pos/get_sample_at_pos}: mismatch smp = " smpFMT " xsmp = " smpFMT, smp1, xsmp); } 
      
        ppv_sample_t smp2 = ransample(d, ixA, 7, +11, A->bps);
        ppv_set_sample(A,ixA, smp2);
        ppv_sample_t ysmp = ppv_get_sample(A, ixA);
        if (ysmp != smp2) 
          { bug("{set_sample/get_sample}: mismatch smp = " smpFMT " ysmp = " smpFMT, smp2, ysmp); } 

        xpos++;
        return FALSE;
      }
    
    bool_t check1(ppv_index_t ixA[])
      { 
        ppv_sample_t smp = ransample(d, ixA, 7, +11, A->bps);
        ppv_pos_t pos = ppv_sample_pos(A, ixA);
        ppv_sample_t xsmp = ppv_get_sample_at_pos(A->el, A->bps, A->bpw, pos);
        if (xsmp != smp) 
          { bug("{get_sample_at_pos} mismatch on 2nd pass smp = " smpFMT " xsmp = " smpFMT, smp, xsmp); } 
        ppv_sample_t ysmp = ppv_get_sample(A, ixA);
        if (ysmp != smp) 
          { bug("{ge_sample}: mismatch smp = " smpFMT " ysmp = " smpFMT, smp, ysmp); } 
        return FALSE;
      }
  }

void test_enum(ppv_array_desc_t *A)
  {
    fprintf(stderr, "!! NOT checking enum yet!\n");
  }
  
void test_assign(ppv_array_desc_t *A, ppv_nbits_t bpsB)
  {
    /* !!! Warning: this test will fail if {A} has replicated elements !!! */
  
    fprintf(stderr, "Checking {ppv_assign} A.bps = %u B.bps = %u ...\n", A->bps, bpsB);
    ppv_dim_t d = A->d;

    /* Choose a {bpw} different from {A}'s: */
    ppv_nbits_t bpwB = (ppv_nbits_t)(A->bpw == 32 ? 8 : 2*A->bpw);
    
    /* Create array {B} with same indices as {A}: */
    ppv_array_desc_t *B = ppv_array_new(d, A->size, bpsB, bpwB);
    fprintf(stderr, "A.el = %016lx B.el = %016lx\n", (uint64_t)A->el, (uint64_t)B->el);
    
    /* Fill {A} and {B} with different garbage: */
    ppv_sample_t maskB = (ppv_sample_t)((1 << bpsB) - 1);

    auto bool_t fillAB(ppv_index_t ixA[]);
    enum_by_hand(fillAB, A);
    
    /* Copy {A} into {B}: */
    ppv_array_assign(B, A);
    
    /* Check whether the assignment succeeded: */
    auto bool_t checkAB(ppv_index_t ixA[]);
    enum_by_hand(checkAB, A);
    
    return;
    
    bool_t fillAB(ppv_index_t ixA[])
      { ppv_sample_t smpA = ransample(d, ixA, 3, +17, A->bps);
        ppv_sample_t smpB = ransample(d, ixA, 5, -23, B->bps);
        ppv_set_sample(A, ixA, smpA);
        ppv_set_sample(B, ixA, smpB);
        return FALSE;
      }
    
    bool_t checkAB(ppv_index_t ixA[])
      { ppv_sample_t smpA_get = ppv_get_sample(A, ixA);
        ppv_sample_t smpB_get = ppv_get_sample(B, ixA);
        ppv_sample_t smpB_ass = smpA_get & maskB;
        if (smpB_get != smpB_ass)
          { ix_print_indices(stderr, "ix = (", d, ixA, 0, " ", ")\n");
            bug("{ppv_assign}: {A} and {B} differ after assignment: B now = " smpFMT " assigned = " smpFMT, smpB_get, smpB_ass);
          }
        ppv_sample_t smpA_set = ransample(d, ixA, 3, +17, A->bps);
        if (smpA_get != smpA_set)
          { ix_print_indices(stderr, "ix = (", d, ixA, 0, " ", ")\n");
            bug("{ppv_assign}: {A} changed set = " smpFMT " get = " smpFMT "\n", smpA_set, smpA_get);
          }
        return FALSE;
      }
  }

void test_crop(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking {ppv_crop}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_step_t sh[d]; /* Shift of cropped array along each axis. */
    ppv_size_t sz[d]; /* Size of cropped array along each axis. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {sh} and {sz} for full array: */
        for (ppv_axis_t ax = 0; ax < d; ax++) { sh[ax] = 0; } 
        memcpy(sz, A->size, d*sizeof(ppv_size_t)); 
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Perform first cropping: */
        ppv_axis_t ax1 = (ppv_axis_t)(trial % d);
        ppv_size_t skip1 = ((trial + trial/2) & 3)*sz[ax1]/8;
        ppv_size_t keep1 = ((trial + trial/4) & 3)*(sz[ax1] - skip1)/8;
        sh[ax1] += skip1; sz[ax1] = keep1;
        ppv_crop(B, ax1, skip1, keep1);
        if (verbose) 
          { fprintf(stderr, "ppv_crop(%d, " szFMT  szFMT ")", ax1, skip1, keep1);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_crop}: returns an invalid desc"); }

        /* Perform second cropping (possibly on same axis): */ 
        ppv_axis_t axa2 = (ppv_axis_t)((trial / d) % d);
        ppv_size_t skip2 = ((trial + trial/2) & 3)*sz[axa2]/8;
        ppv_size_t keep2 = ((trial + trial/5) & 3)*(sz[axa2] - skip2)/8;
        sh[axa2] += skip2; sz[axa2] = keep2;
        ppv_crop(B, axa2, skip2, keep2);
        if (verbose) 
          { fprintf(stderr, "ppv_crop(%d, " szFMT ", " szFMT ")", axa2, skip2, keep2);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_crop}: returns an invalid desc"); }

        /* Check consistency of storage area: */
        if ((keep1 == 0) || (keep2 == 0))
          { if (B->el != NULL) { bug0("{ppv_crop}: empty array with non-null storage"); } }
        else
          { if (B->el != A->el) { bug0("{ppv_crop}: garbled storage area"); } }
        /* Check whether the size of {B} is correct: */
        check_size(d, B->size, sz);
        /* Now check coincidence of the two arrays: */
        
        auto bool_t check1(ppv_index_t ixB[]);
        enum_by_hand(check1, B);

        continue;

        /* Internal procedures: */

        bool_t check1(ppv_index_t ixB[])
          { 
            ppv_index_t ixA[d];
            ppv_index_assign(d, ixA, ixB); ppv_index_shift(d,ixA, sh);
            ppv_pos_t posB = ppv_sample_pos(B, ixB);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posB != posA)
              { bug("{ppv_crop}: position error posB = " posFMT " posA = " posFMT, posB, posA); }
            return FALSE;
          }

      }
        
    return;
  }

void test_subsample(ppv_array_desc_t *A) 
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking {ppv_subsample}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_size_t st[d]; /* Subsampling step along each axis. */
    ppv_size_t sz[d]; /* Size of subsampled array along each axis. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {st} and {sz} for full array: */
        for (ppv_axis_t ax = 0; ax < d; ax++) { st[ax] = 1; } 
        memcpy(sz, A->size, d*sizeof(ppv_size_t)); 
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Perform first subsampling: */
        ppv_axis_t ax1 = (ppv_axis_t)(trial % d);
        ppv_size_t step1 = 1 + (trial & 3)*(sz[ax1] - 1)/8;
        ppv_size_t keep1 = (sz[ax1] + step1 - 1)/step1;
        st[ax1] *= step1; sz[ax1] = keep1;
        ppv_subsample(B, ax1, step1);
        if (verbose) 
          { fprintf(stderr, "ppv_subsample(%d, " szFMT ")", ax1, step1);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_subsample}: returns an invalid desc"); }

        /* Perform second subsampling (possibly on same axis): */ 
        ppv_axis_t axa2 = (ppv_axis_t)((trial / d) % d);
        ppv_size_t step2 = 1 + (trial/2 & 3)*(sz[axa2] - 1)/8;
        ppv_size_t keep2 = (sz[axa2] + step2 - 1)/step2;
        st[axa2] *= step2; sz[axa2] = keep2;
        ppv_subsample(B, axa2, step2);
        if (verbose) 
          { fprintf(stderr, "ppv_subsample(%d, " szFMT ")", axa2, step2);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_subsample}: returns an invalid desc"); }

        /* Check consistency of storage area: */
        if (B->el != A->el) { bug0("{ppv_subsample}: garbled storage area"); }
        /* Check whether the size of {B} is correct: */
        check_size(d, B->size, sz); 
        /* Now check coincidence of the two arrays: */
        
        auto bool_t check1(ppv_index_t ixB[]);
        enum_by_hand(check1, B);

        return;
        
        bool_t check1(ppv_index_t ixB[])       
          { ppv_index_t ixA[d];
            for (ppv_axis_t ax = 0; ax < d; ax++) { ixA[ax] = ixB[ax]*st[ax]; }
            ppv_pos_t posB = ppv_sample_pos(B, ixB);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posB != posA)
              { bug("{ppv_subsample}: position error posB = " posFMT " posA = " posFMT, posB, posA); } 
            return FALSE;
          }
      }
  }

void test_swap_indices(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    if (d < 2)
      { fprintf(stderr, "NOT checking {ppv_swap_indices}: {d} too small\n");
        return;
      }
    fprintf(stderr, "Checking {ppv_swap_indices}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_size_t tr[d]; /* Axis {ax} of {B} is axis {tr[ax]} of {A}. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {tr} with identity permutation: */
        for (ppv_axis_t ax = 0; ax < d; ax++) { tr[ax] = ax; }
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        for (int32_t pass = 0; pass < 2; pass++)
          { /* Perform a transposition: */
            ppv_axis_t axa1 = (ppv_axis_t)((trial/(1+pass)) % d);
            ppv_axis_t axb1 = (ppv_axis_t)((trial/(2+pass)) % d);
            int d1 = (axa1 < axb1 ? axb1 - axa1 : axa1 - axb1);
            int m1 = d - (axa1 > axb1 ? axa1 : axb1);
            ppv_dim_t n1 = (ppv_dim_t)((trial/(3+pass)) % (d1 == 0 ? m1 : (d1 < m1 ? d1 : m1)));
            assert(axa1 + n1 <= d);
            assert(axb1 + n1 <= d);
            assert((axa1 == axb1) || (axa1 + n1 <= axb1) || (axb1 + n1 <= axa1));
            for (int32_t k = 0; k < n1; k++)
              { ppv_size_t tmp = tr[axa1+k]; tr[axa1+k] = tr[axb1+k]; tr[axb1+k] = tmp; } 
            ppv_swap_indices(B, axa1, axb1, n1);
            if (verbose) 
              { fprintf(stderr, "ppv_swap_indices(%d, %d, %d)", axa1, axb1, n1);
                ppv_print_descriptor(stderr, " = { ", B, " }\n");
              } 
            /* Check validity of descriptor: */
            if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_swap_indices}: returns an invalid desc"); }
          }

        /* Check consistency of storage area: */
        if (B->el != A->el) { bug0("{ppv_swap_indices}: garbled storage area"); }
        for (ppv_axis_t ax = 0; ax < d; ax++)
          { /* Check whether the size of {B} is correct: */
            if (B->size[ax] != A->size[tr[ax]]) 
              { bug("size[%d] = " szFMT, ax, B->size[ax]); }
            /* Check whether the increments of {B} are correct: */
            if (B->step[ax] != A->step[tr[ax]]) 
              { bug("step[%d] = " stFMT, ax, B->step[ax]); }
          }
        /* Now check coincidence of the two arrays: */
        auto bool_t check1(ppv_index_t ixB[]);
        enum_by_hand(check1, A);
        
        return;
        
        bool_t check1(ppv_index_t ixA[])       
          { ppv_index_t ixB[d];
            for (ppv_axis_t ax = 0; ax < d; ax++) { ixB[ax] = ixA[tr[ax]]; }
            ppv_pos_t posB = ppv_sample_pos(B, ixB);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posB != posA)
              { bug("{ppv_swap_indices}: pos error posB = " posFMT " posA = " posFMT, posB, posA); } 
            return FALSE;
          }
      }
  }

void test_flip_indices(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking {ppv_flip_indices}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_size_t tr[d]; /* Axis {ax} of {B} is axis {tr[ax]} of {A}. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {tr} with identity permutation: */
        for (ppv_axis_t ax = 0; ax < d; ax++) { tr[ax] = ax; }
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Perform first transposition: */
        ppv_axis_t axa1 = (ppv_axis_t)(trial % d);
        ppv_axis_t axb1 = (ppv_axis_t)(axa1 + (trial/2 % (d-axa1)));
        { ppv_axis_t axi=axa1, axj=axb1;
          while (axi < axj) 
            { ppv_size_t tmp = tr[axi]; tr[axi] = tr[axj]; tr[axj] = tmp; axi++; axj--; }
        }
        ppv_flip_indices(B, axa1, axb1);
        if (verbose) 
          { fprintf(stderr, "ppv_flip_indices(%d, %d)", axa1, axb1);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_flip_indices}: returns an invalid desc"); }

        /* Perform second transposition (possibly on same axis): */ 
        ppv_axis_t axa2 = (ppv_axis_t)((trial/d) % d);
        ppv_axis_t axb2 = (ppv_axis_t)(axa2 + ((trial/d/2) % (d-axa2)));
        { ppv_axis_t axi=axa2, axj=axb2;
          while (axi < axj) 
            { ppv_size_t tmp = tr[axi]; tr[axi] = tr[axj]; tr[axj] = tmp; axi++; axj--; }
        }
        ppv_flip_indices(B, axa2, axb2);
        if (verbose) 
          { fprintf(stderr, "ppv_flip_indices(%d, %d)", axa2, axb2);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_flip_indices}: returns an invalid desc"); }

        /* Check consistency of storage area: */
        if (B->el != A->el) { bug0("{ppv_flip_indices}: garbled storage area"); }
        for (ppv_axis_t ax = 0; ax < d; ax++)
          { /* Check whether the size of {B} is correct: */
            if (B->size[ax] != A->size[tr[ax]]) 
              { bug("size[%d] = " szFMT, ax, B->size[ax]); }
            /* Check whether the increments of {B} are correct: */
            if (B->step[ax] != A->step[tr[ax]]) 
              { bug("step[%d] = " stFMT, ax, B->step[ax]); }
          }
        /* Now check coincidence of the two arrays: */
        
        auto bool_t check1(ppv_index_t ixA[]);
        enum_by_hand(check1, A);

        return;

        bool_t check1(ppv_index_t ixA[])       
          { 
            ppv_index_t ixB[d];
            for (ppv_axis_t ax = 0; ax < d; ax++) { ixB[ax] = ixA[tr[ax]]; }
            ppv_pos_t posB = ppv_sample_pos(B, ixB);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posB != posA)
              { bug("{ppv_flip_indices}: pos error posB = " posFMT " posA = " posFMT, posB, posA); } 
            return FALSE;
          }
      }
  }

void test_reverse(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking {ppv_reverse}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    bool_t fp[d]; /* Tells whether each axis was flipped or not. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {fp} for unflipped array: */
        for (ppv_axis_t ax = 0; ax < d; ax++) { { fp[ax] = FALSE; } }
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Perform first flip: */
        ppv_axis_t ax1 = (ppv_axis_t)(trial % d);
        fp[ax1] = ! fp[ax1];
        ppv_reverse(B, ax1);
        if (verbose) 
          { fprintf(stderr, "ppv_reverse(%d)", ax1);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_reverse}: returns an invalid desc"); }

        /* Perform second flip (possibly on same axis): */ 
        ppv_axis_t ax2 = (ppv_axis_t)((trial / d) % d);
        fp[ax2] = ! fp[ax2];
        ppv_reverse(B, ax2);
        if (verbose) 
          { fprintf(stderr, "ppv_reverse(%d)", ax2);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_reverse}: returns an invalid desc"); }

        /* Check consistency of storage area: */
        if (B->el != A->el) { bug0("{ppv_reverse}: garbled storage area"); }
        /* Check whether the size of {B} is correct: */
        check_size(d, B->size, A->size);
        /* Check whether the increments of {B} are correct: */
        { for (ppv_axis_t ax = 0; ax < d; ax++) 
            if (B->step[ax] != A->step[ax]*(fp[ax]?-1:+1)) 
              { bug("step[%d] =" stFMT, ax, B->step[ax]); }
        }
        /* Now check coincidence of the two arrays: */
        auto bool_t check1(ppv_index_t ixB[]);
        enum_by_hand(check1, A);
        
        return;
        
        bool_t check1(ppv_index_t ixB[])       
          { ppv_index_t ixA[d];
            for (ppv_axis_t ax = 0; ax < d; ax++) 
              { ixA[ax] = (fp[ax] ? A->size[ax] - 1 - ixB[ax] : ixB[ax]); }
            ppv_pos_t posB = ppv_sample_pos(B, ixB);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posB != posA)
              { bug("{ppv_reverse}: position error posB = " posFMT " posA = " posFMT, posB, posA); }
            return FALSE;
          }
                    
      }
  }

void test_slice(ppv_array_desc_t *A)
  {
    fprintf(stderr, "!! NOT checking {ppv_slice}...\n");
  }

void test_diagonal(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    if (d < 2)
      { fprintf(stderr, "NOT checking {ppv_diagonal}: {d} too small\n");
        return;
      }
    fprintf(stderr, "Checking {ppv_diagonal}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_size_t sz[d]; /* Size of diagonalized array along each axis. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {sz} for full array: */
        memcpy(sz, A->size, d*sizeof(ppv_size_t)); 
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Choose any two distinct axes {axa1,axb1}: */
        ppv_axis_t axa1 = (ppv_axis_t)(trial % d);
        ppv_axis_t axb1 = (ppv_axis_t)(trial/d % (d-1));
        if (axb1 >= axa1) { axb1++; }
        /* Make sure {sz[axa1] <= sz[axb1]}: */
        if (sz[axa1] > sz[axb1]) { ppv_axis_t t = axa1; axa1 = axb1; axb1 = t; }
        if (sz[axa1] > 0)
          { 
            /* Take diagonal slice of array: */
            ppv_diagonal(B, axa1, axb1);
            if (verbose) 
              { fprintf(stderr, "ppv_diagonal(%d, %d)", axa1, axb1);
                ppv_print_descriptor(stderr, " = { ", B, " }\n");
              } 
            /* Compute expected sizes of {1}: */
            sz[axb1] = sz[axb1] - (sz[axa1] - 1);
            /* Check validity of descriptor: */
            if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_diagonal}: returns an invalid desc"); }

            /* Check consistency of storage area: */
            if (B->el != A->el) { bug0("{ppv_diagonal}: garbled storage area"); }
            /* Check whether the size of {1} is correct: */
            check_size(d, B->size, sz);
            /* Now check coincidence of the two arrays: */
            auto bool_t check1(ppv_index_t ixB[]);
            enum_by_hand(check1, B);

            return;

            bool_t check1(ppv_index_t ixB[])       
              { ppv_index_t ixA[d];
                ppv_index_assign(d, ixA, ixB);
                ixA[axb1] += ixA[axa1];
                ppv_pos_t posB = ppv_sample_pos(B, ixB);
                ppv_pos_t posA = ppv_sample_pos(A, ixA);
                if (posB != posA)
                  { bug("{ppv_diagonal}: position error posB = " posFMT " posA = " posFMT, posB, posA); }
                return FALSE;
              }
          }
      }
  }

void test_chop(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking {ppv_chop}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_size_t sz[d]; /* Size of chopped array along each axis. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {sz} for full array: */
        memcpy(sz, A->size, d*sizeof(ppv_size_t)); 
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Choose an axis to chop: */
        ppv_axis_t axa1 = (ppv_axis_t)(trial % d);
        /* Choose a nonzero chunk size: */
        ppv_size_t chunksz1 = 1 + (trial/3 & 3)*(B->size[axa1] - 1)/8;
        /* Ensure that {B->size[axa1]} is a multiple of {chunksz1}: */
        if ((B->size[axa1] % chunksz1) != 0)
          { if (chunksz1 > B->size[axa1]) 
              { chunksz1 = (B->size[axa1] + 1)/2; }
            ppv_crop(B, axa1, 0, (B->size[axa1]/chunksz1)*chunksz1);
          }
        /* Chop array: */
        ppv_array_desc_t *C = ppv_chop(B, axa1, chunksz1);
        assert(C->d == B->d + 1);
        ppv_axis_t axb1 = (ppv_axis_t)(C->d - 1); /* Chunk index axis. */
        /* Compute expected sizes of {C}: */
        sz[axb1] = sz[axa1]/chunksz1; sz[axa1] = chunksz1;
        if (verbose) 
          { fprintf(stderr, "ppv_chop(%d, " szFMT ", %d)", axa1, chunksz1, axb1);
            ppv_print_descriptor(stderr, " = { ", C, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(C, TRUE)) { bug0("{ppv_chop}: returns an invalid desc"); }

        /* Check consistency of storage area: */
        if (C->el != A->el) { bug0("{ppv_chop}: garbled storage area"); }
        /* Check whether the size of {1} is correct: */
        check_size(d, C->size, sz);
        /* Now check coincidence of the two arrays: */
        auto bool_t check1(ppv_index_t ixA[]);
        enum_by_hand(check1, A);

        return;

        bool_t check1(ppv_index_t ixA[])       
          { ppv_index_t ixC[d];
            ppv_index_assign(d, ixC, ixA);
            ixC[axa1] = ixA[axa1] % chunksz1;
            ixC[axb1] = ixA[axa1] / chunksz1;
            ppv_pos_t posC = ppv_sample_pos(C, ixC);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posC != posA)
              { bug("{ppv_chop}: pos error posC = " posFMT " posA = " posFMT, posC, posA); } 
            return FALSE;
          }
      }
  }

void test_replicate(ppv_array_desc_t *A)
  {
    ppv_dim_t d = A->d;
    fprintf(stderr, "Checking {ppv_replicate}...\n");
    bool_t verbose = FALSE;
    if (verbose) { ppv_print_descriptor(stderr, "A = { ", A, " }\n"); } 
    int32_t ntrials = 2*d*d;
    ppv_size_t sz[d]; /* Size of replicated array along each axis. */
    for (int32_t trial = 0; trial < ntrials; trial++)
      { /* Initialze {sz} for full array: */
        memcpy(sz, A->size, d*sizeof(ppv_size_t)); 
        /* Start with the standard array: */
        ppv_array_desc_t *B = ppv_array_clone(A);

        /* Choose axis for first replication: */
        ppv_axis_t ax1 = (ppv_axis_t)(trial % d);
        /* Choose a positive replication factor: */
        ppv_size_t rep1 = 1 + (trial/3 & 3)*5;
        /* Chop and replicate along axis {ax1}: */
        ppv_size_t skip1 = B->size[ax1]/2;
        ppv_crop(B, ax1, skip1, 1);
        ppv_replicate(B, ax1, rep1);
        sz[ax1] = rep1;
        if (verbose) 
          { fprintf(stderr, "ppv_crop(%d," szFMT ",1)+ppv_replicate(%d," szFMT ")", ax1, skip1, ax1, rep1);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_replicate}: returns an invalid desc"); }

        /* Choose axis for second replication (may be {ax1}): */
        ppv_axis_t ax2 = (ppv_axis_t)((trial/d) % d);
        /* Choose a replication factor: */
        ppv_size_t rep2 = 1 + (trial/8 & 3)*5;
        /* Chop and replicate along axis {ax2}: */
        ppv_size_t skip2 = B->size[ax2]/2;
        ppv_crop(B, ax2, skip2, 1);
        ppv_replicate(B, ax2, rep2);
        sz[ax2] = rep2;
        if (verbose) 
          { fprintf(stderr, "ppv_crop(%d," szFMT ",1)+ppv_replicate(%d," szFMT ")", ax2, skip2, ax2, rep2);
            ppv_print_descriptor(stderr, " = { ", B, " }\n");
          } 
        /* Check validity of descriptor: */
        if (! ppv_descriptor_is_valid(B, TRUE)) { bug0("{ppv_replicate}: returns an invalid desc"); }

        /* Check consistency of storage area: */
        if (B->el != A->el) { bug0("{ppv_replicate}: garbled storage area"); }
        /* Check whether the size of {1} is correct: */
        check_size(d, B->size, sz);
        /* Now check coincidence of the two arrays: */
        auto bool_t check1(ppv_index_t ixB[]);
        enum_by_hand(check1, B);

        return;

        bool_t check1(ppv_index_t ixB[])       
          { ppv_index_t ixA[d];
            ppv_index_assign(d, ixA, ixB);
            ixA[ax1] = skip1;
            /* The second crop changes {base} only if {ax1 != ax2}: */
            if (ax2 != ax1) { ixA[ax2] = skip2; }
            ppv_pos_t posB = ppv_sample_pos(B, ixB);
            ppv_pos_t posA = ppv_sample_pos(A, ixA);
            if (posB != posA)
              { bug("{ppv_replicate}: position error posB = " posFMT " posA = " posFMT, posB, posA); }
            return FALSE;
          }
      }
  }

void dump_storage(FILE *wr, void *el, int nw, ppv_nbits_t bps, ppv_nbits_t bpw)
  {
    if (bps == 0)
      { fprintf(stderr, "  zero-length pixels -- no storage area to dump\n");
        return;
      }
    if (el == NULL)
      { fprintf(stderr, "  array has no storage area\n");
        return;
      }
    for (int32_t iw = 0; iw < nw; iw++)
      { ppv_word_t w;
        if (bpw == 8) 
            { w = *(((ppv_word_08_t *)el) + iw); }
          else if (bpw == 16) 
            { w = *(((ppv_word_16_t *)el) + iw); }
          else 
            { w = *(((ppv_word_32_t *)el) + iw); }
        if (iw != 0) { fprintf(wr, " "); }
        for (int32_t axb = bpw-1; axb >= 0; axb--)
          { fprintf(wr, "%d", (w >> axb) & 1); }
      }
  }

void check_size(ppv_dim_t d, ppv_size_t *sza, ppv_size_t *szb)
  {
    for (ppv_axis_t ax = 0; ax < d; ax++)
      { if (sza[ax] != szb[ax]) { bug("size[%d] = " szFMT, ax, sza[0]); } }
  }
